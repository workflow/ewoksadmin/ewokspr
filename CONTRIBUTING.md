# Contributing to `ewoksdoc`

## Quickstart

```bash
pip install -e .[full]
python scripts/generate.py
sphinx-build doc build
```

`pip install -e .[full]` install the requirements to build the doc **plus** the `ewoks` projects providing the tasks so that they can be discovered by the following step.

`python scripts/generate.py` generates MD files in the `doc` folder that lists the tasks, categories and beamlines of the projects defined in the preamble of `scripts/generate.py`.

`sphinx-build doc build` generates HTML documentation in `build`.

## Write documentation

The documentation is composed of Markdown files located in `doc`. If a new file is created, don't forget to reference it in one of the `toctree` directive.

## Build documentation

The documentation is built with [Sphinx](https://www.sphinx-doc.org/en/master/index.html) with the help of the following projects:

- [myst-parser](https://myst-parser.readthedocs.io/en/latest/index.html) to allow Sphinx to parse Markdown files
- [sphinx-book-theme](https://sphinx-book-theme.readthedocs.io/en/stable/) as the Sphinx theme
- [sphinx-design](https://sphinx-design.readthedocs.io/en/latest/) to write Sphinx admonitions rendering as modern UI components.

The documentation can be built using the process described in _Quickstart_. When developing/writing doc, [sphinx-autobuild](https://github.com/executablebooks/sphinx-autobuild) can be used to automatically rebuild the documentation on changes

```
sphinx-autobuild doc build
```

The dynamic build will then be served on http://127.0.0.1:8000/.
