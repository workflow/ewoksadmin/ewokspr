# Extensible Workflow System (Ewoks)

**Ewoks** is a project to automate data processing and experiments at large-scale facilities
as well as making data processing more scientific (reproducible) and [FAIR](https://www.go-fair.org) (traceable).

::::{grid} 2
:gutter: 2

:::{grid-item-card}
:link: getting_started.html

**New user of Ewoks?**
^^^
The **Getting started** page will give instructions to start using Ewoks on a local computer.
:::
:::{grid-item-card}
:link: tutorials/index.html

**You know about Ewoks and want to start creating workflows?**
^^^
The **Tutorials** page provide guided instructions to create and execute Ewoks workflows.

⚠️ Still in construction!
:::
:::{grid-item-card}
:link: tasks/index.html

**You know how to create Ewoks workflows and are looking for existing Ewoks tasks?**
^^^
The **Task catalog** is the page to check: it provides a searchable list of tasks, regrouped by experimental techniques and projects.
:::
:::{grid-item-card}
:link: press.html

**Want to know more about Ewoks in general?**
^^^
To get a bird's eye view of Ewoks, you can have a look at the articles and presentations in the **Press** page. Or **keep scrolling** for more info!
:::
::::

## What is Ewoks?

Ewoks is a collection of Python projects to design and run data processing via workflows.

The Ewoks ecosystem is composed of:

- **ewoks** to submit and execute workflows with any supported execution engine, be it locally or remotely.
- **ewokscore** that defines the specification of Ewoks workflows.
- **ewoksweb**: a web service to create, edit and execute workflows.
- several bindings for workflow management systems, depending on the use case.
- several **ewoksapps** exists that define workflows and tasks for specific domains (ex: `tomwer` for tomography, `ewoksxrpd` for X-Ray powder diffraction data processing).

**All Ewoks projects are open-source and can be found on the ESRF Gitlab in the [ewoks](https://gitlab.esrf.fr/workflow/ewoks) and [ewoksapps](https://gitlab.esrf.fr/workflow/ewoksapps) subgroups.**

:::{admonition} Example
:class: tip

Ewoks is used at ESRF beamlines to azimuthally integrate diffraction images automatically during acquisition.

**But it can do much more!** See the [task catalog](tasks/index.md) for a list of possible operations (to be expanded soon with a workflow catalog).
:::

## Features

Ewoks is not yet another [workflow management system](https://s.apache.org/existing-workflow-systems).
To ensure the longevity of workflows and their implementation, Ewoks was designed to be a
_meta workflow system_. This allows workflows to be isolated from the underlying software technologies
used to execute and manage them.

The _meta workflow_ approach allows for supporting a diverse set of use cases tailored to individual
scientists on one end of the spectrum and large-scale facilities on the other end.

- automated workflows distributed on a compute cluster
- interactive worfkflows run as a desktop application
- create, manage and execute workflows as a web service
- workflows with loops and conditional links
- workflow tasks written in Python or defined by command line tools
- parallel execution of tasks within the same workflow
- job scheduling for integration in other systems (acquisition control, data portal)

## How to cite Ewoks?

[![][doi-shield]][doi]

[doi]: https://doi.org/10.1080/08940886.2024.2432305
[doi-shield]: https://img.shields.io/badge/DOI-10.1080/08940886.2024.2432305-blue

:::{toctree}
:hidden:

getting_started
engines
tutorials/index
howtoguides
tasks/index
related
press
:::
