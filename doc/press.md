Press
=====

* [Synchrotron Radiation News 2024](https://doi.org/10.1080/08940886.2024.2432305)
* [NOBUGS 2024](https://indico.esrf.fr/event/114/)
    * [talk](https://indico.esrf.fr/event/114/contributions/773/)
    * [talk](https://indico.esrf.fr/event/144/#5-ewoks)
    * [demo](https://indico.esrf.fr/event/137/#4-ewoks-demonstration)
* [MLZ Workshop 2024](https://indico.frm2.tum.de/event/439/contributions/4902/attachments/1027/1776/2024_03_mlz_workshop_wdn.pdf)
* [ESRF Highlights 2023](https://www.esrf.fr/Apache_files/Highlights/2023/index.html#/page/172)
* [ESRF Newsletter December 2023](https://www.esrf.fr/Apache_files/Newsletter/2023/December/index.html#/page/18)
* [NOBUGS 2022](https://www.youtube.com/watch?v=3bJqPABAW8M)
