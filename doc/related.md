# Related projects

This page gathers Python libraries that are related to or used by Ewoks

## Data Access: `blissdata`

[blissdata](https://bliss.gitlab-pages.esrf.fr/bliss/master/blissdata/intro.html) handles the access of data stored and managed by [BLISS](https://gitlab.esrf.fr/bliss/bliss), the ESRF beamline control system.

## Online Data Analysis in Bliss: `blissoda`

[blissoda](https://blissoda.readthedocs.io) blissoda provides utilities for online data analysis in BLISS.

It does not provide code for data processing but contains the BLISS-side logic of online data processing.

## Upload Results to ICAT: `pyicat-plus`

[pyicat-plus](https://pyicat-plus.readthedocs.io) is a Python client for [ICAT+](https://icat.gitlab-pages.esrf.fr/icat-plus/) that can be used to register results.

Ewoks uses it to register processed data to ICAT+ so that the data is then accessible in the ESRF data portal.

## Slurm API in python: `pyslurmutils`

[pyslurmutils](https://pyslurmutils.readthedocs.io) provides utilities to submit jobs to SLURM over the SLURM REST API.
