# Supported Ewoks engines

Ewoks workflows can be executed with the following engines:
- [dask](https://www.dask.org/)
- [pypushflow](https://pypushflow.readthedocs.io/en/latest/): a task scheduler for acyclic and cyclic graphs
- [Orange](https://orangedatamining.com/): a tool for visual programming and data visualisation

## Use a supported engine

To use any of these engines, you will need to install the associated package:

```bash
pip install ewoksdask # for dask
pip install ewoksppf # for pypushflow
pip install ewoksorange # for Orange
```

Note that Ewoks provides a basic sequential execution engine to be used by default if none of these packages are installed.

To specify the engine used for the execution, use the `--engine` parameter of `ewoks execute` (here for `dask`):

```bash
ewoks execute --test demo --engine dask
```

:::{warning}

Orange works differently since the execution can only be triggered from the Orange GUI.

Running the following command will **not** execute the graph but opens the Orange GUI for workflow edition.

```{code} bash
ewoks execute --test demo --engine orange
```

To execute the workflow, double-click on `task0` to make to Task widget appear and click on _Trigger_. For more information on how to use the Orange GUI, see the [Orange documentation](https://orangedatamining.com/getting-started/).

:::

## Engine comparison

Depending on the engine used, some features may or may not be available. For example, workflows with conditional links or loops can only be used with `pypushflow`. 

The following table sums up the difference between each engine


| engine     | Loops                     | Conditional Links         | Parallel execution           | Interaction (GUI)         | Native support   |
|------------|-------------------------------------|-------------------------------------|----------------------------------------|---------------------------|---------------------------|
| `None`     | <span style='color: red'>✗</span>   | <span style='color: red'>✗</span>   | <span style='color: red'>✗</span>      | <span style='color: red'>✗</span>   | <span style='color: green'>✓</span> |
| `"dask"`   | <span style='color: red'>✗</span>   | <span style='color: red'>✗</span>   | <span style='color: green'>✓</span>    | <span style='color: red'>✗</span>   | <span style='color: green'>✓</span> |
| `"ppf"`    | <span style='color: green'>✓</span> | <span style='color: green'>✓</span> | <span style='color: green'>✓</span>    | <span style='color: red'>✗</span>   | <span style='color: green'>✓</span> |
| `"orange"` | <span style='color: red'>✗</span>   | <span style='color: red'>✗</span>   | <span style='color: orange'>✓</span> | <span style='color: green'>✓</span> | <span style='color: red'>✗</span>   |

:::{note}

**Native support** means that Ewoks tasks can be run by this engine without any needed modification. 

In the case of Orange, since execution goes through the GUI, Orange widgets need to be created and associated to the Ewoks task to use it in Orange. This is covered by the [Orange widget creation tutorial](https://ewoksorange.readthedocs.io/en/stable/tutorials/my_first_widget/index.html) of the `ewoksorange` documentation.

:::

## Add a new engine to Ewoks

So far, there is no tutorial to add a new engine to Ewoks since it is heavily dependent on the execution engine itself. In essence, it consists in creating bindings that describe how the execution engine executes workflows.

A good starting point is to look at the [ewoksdask](https://gitlab.esrf.fr/workflow/ewoks/ewoksdask) project. The [bindings.py](https://gitlab.esrf.fr/workflow/ewoks/ewoksdask/-/blob/main/src/ewoksdask/bindings.py) file exposes a `execute_graph` function that is registered in Ewoks through the `execute_graph` decorator. This is the only thing needed to register the engine in Ewoks.

Then, by implementing the workflow execution for a specific execution engine and registering it with the same pattern, one can add support for this execution engine.
