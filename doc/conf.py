# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import os

project = "ewoksdoc"
copyright = "2023-2025, ESRF"
author = "ESRF"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["myst_parser", "sphinx_design"]
myst_enable_extensions = ["colon_fence", "fieldlist"]

templates_path = ["_templates"]
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output
# https://pydata-sphinx-theme.readthedocs.io/en/stable/user_guide/index.html

html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]
html_logo = "_static/ewoks.png"
html_title = "Extensible Workflow System"
html_theme_options = {
    "logo": {
        "alt_text": "EWOKS",
        "image_dark": "_static/ewoks-dark.png",
    },
    "footer_start": ["copyright"],
    "footer_end": ["footer_end"],
}

# -- Options for RTD -------------------------------------------------
# https://about.readthedocs.com/blog/2024/07/addons-by-default/

# Define the canonical URL for the custom domain on Read the Docs
html_baseurl = os.environ.get("READTHEDOCS_CANONICAL_URL", "")

html_context = {}
# Tell Jinja2 templates the build is running on Read the Docs
if os.environ.get("READTHEDOCS", "") == "True":
    html_context["READTHEDOCS"] = True
