# Getting started


## What is Ewoks ?

**Ewoks is an ecosystem of Python packages made to execute computational graphs** (also called workflows). Workflows are graphs where each node represents a processing step and each link represents data flowing from one step to the next.

:::{image} https://gitlab.esrf.fr/workflow/ewokstutorials/ewoksfordevs/-/raw/main/images/workflow1.excalidraw.svg?ref_type=heads

:::

Such workflows can be stored in JSON files, YAML files or generated in Python according to [the Ewoks specification](https://ewokscore.readthedocs.io/en/latest/definitions.html).

**Ewoks works as a reconciling layer between workflow systems**: any workflow system can be integrated in Ewoks to be then used to execute workflows that follow the Ewoks specification. The supported workflow systems are also called _execution engines_: see the [Engines](./engines.md) page for more information.

## Quick start

The `ewoks` meta-package gives all the necessary tools to manipulate Ewoks workflows. It can be installed with `pip`:

```bash
pip install ewoks
```

The `ewoks` package comes with several example workflows, ready to be run.

For example, the `demo` workflow can be run in the command line with `ewoks execute`:

```bash
ewoks execute demo -p a=10 -p b=3 --test --outputs=all
```

`-p a=10 -p b=3` changes the input parameters, ` --outputs=all` displays the output of each node in the console.

For more information on the command line parameters, run `ewoks execute -h` or see the [Ewoks commands reference](https://ewoks.readthedocs.io/en/stable/cli.html).

---

The `demo` workflow can be inspected by converting it with `ewoks convert` (here in JSON format):

```bash
ewoks convert demo demo.json --test
```

:::{admonition} To go further

To have an example of workflow creation in Python, you can have a look at the [Hello world example](https://ewoks.readthedocs.io/en/stable/hello_world.html) of Ewoks.

Ewoks workflows can also be created using [graphical user interfaces](https://ewoks.readthedocs.io/en/stable/howtoguides/gui.html).

For specific tasks, see our [How-to guides](./howtoguides.md)

For a more step-focused approach, you can check the
[Ewoks tutorial for developers](https://ewoksfordevs.readthedocs.io).
Concepts like workflows, tasks, and the different Ewoks projects are
introduced progressively throughout the slides.

ℹ️ _This tutorial is given to ESRF staff on a monthly basis so the material
continues to evolve._

:::
