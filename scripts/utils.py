import re
from typing import Dict, List, Optional
from importlib import metadata


PROJECT_TO_CATEGORY: Dict[str, str] = {
    "ewoksxrpd": "SAXS/WAXS",
    "ewoksfluo": "Fluorescence",
    "ewoksndreg": "Imaging",
    "ewokscore": "Demo",
    "tomwer": "Tomography",
    "darfix": "Dark-field Microscopy",
    "bes": "MX Beamline Automation",
    "ewoksixs": "Inelastic Scattering",
    "ewoksid22": "Custom Diffraction",
    "ewoksid11": "Custom Diffraction",
    "ewoksid31": "Custom Diffraction",
    "est": "Spectroscopy",
    "ewoksdata": "Data Access",
}


CATEGORY_TO_BEAMLINES: Dict[str, List[str]] = {
    "Tomography": ["BM05", "ID11", "ID16B", "ID17", "BM18", "ID19"],
    "SAXS/WAXS": ["BM02", "ID11", "ID09", "ID16B", "ID31"],
    "Spectroscopy": ["BM23", "ID24"],
    "Fluorescence": ["ID16b", "ID21"],
    "Dark-field Microscopy": ["ID06", "ID11"],
    "Imaging": ["ID16b", "ID21"],
    "MX Beamline Automation": ["ID23-1", "ID23-2", "ID30A-1", "ID30A-3", "ID30B"],
    "BioSAXS": ["BM29"],
    "Custom Diffraction": ["ID11", "ID22", "ID31"],
    "Data Access": [],
    "Demo": [],
    "Development": ["ID01", "ID10", "BM16", "ID26"],
}


def extract_url(project_metadata: dict) -> Optional[str]:
    for s in project_metadata.get("project_url", ()):
        doctype, url = s.split(",")
        if "documentation" in doctype.lower():
            return url.strip()
    return project_metadata.get("home_page")


def get_projects(category: str):
    return sorted(
        {
            project
            for project, _category in PROJECT_TO_CATEGORY.items()
            if category == _category
        }
    )


def category_filename_stem(category: str):
    return re.sub(r"[^-0-9a-z]", "_", category.lower())


def get_project_link(project: str):
    try:
        project_metadata = metadata.metadata(project)
        url = extract_url(project_metadata.json)
        return f"**[{project}]({url})**"
    except ImportError:
        return f"**{project}**"


def get_project_links(projects: List[str]):
    return [get_project_link(p) for p in projects]
