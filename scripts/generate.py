import json
import os
from tabulate import tabulate
from typing import Dict, List
from jinja2 import Environment, FileSystemLoader, select_autoescape

from ewokscore.task_discovery import discover_all_tasks

from utils import (
    CATEGORY_TO_BEAMLINES,
    PROJECT_TO_CATEGORY,
    category_filename_stem,
    get_project_links,
    get_projects,
)

loader = FileSystemLoader(f"{os.path.join(os.path.dirname(__file__), 'templates')}")
env = Environment(
    loader=loader,
    autoescape=select_autoescape(),
    trim_blocks=True,
    lstrip_blocks=True,
)


def create_task_list() -> Dict[str, List[Dict[str, str]]]:

    wd = os.path.dirname(__file__)

    if os.path.exists(os.path.join(wd, "tasks.json")):
        with open(os.path.join(wd, "tasks.json")) as f:
            tasks = json.load(f)

        return tasks

    tasks = {category: list() for category in CATEGORY_TO_BEAMLINES}
    for task in discover_all_tasks():
        project = task["category"]
        category = PROJECT_TO_CATEGORY.get(project, "Miscellaneous")
        description = task["description"]
        if description:
            description = description.split("\n")[0]
        else:
            description = ""
        category_tasks = tasks.setdefault(category, list())

        identifier = task["task_identifier"]
        if task["task_type"] == "ppfmethod":
            name = identifier.split(".")[-2]
        else:
            name = identifier.split(".")[-1]

        if name.startswith("_"):
            continue

        category_tasks.append({"name": name, **task})

    with open(os.path.join(wd, "tasks.json"), "w") as f:
        json.dump(tasks, f)

    return tasks


def save_category_pages(tasks: Dict[str, List[Dict[str, str]]], docdir: str) -> None:
    template = env.get_template("category_tasks.md")
    template.globals["tabulate"] = tabulate
    template.globals["get_projects"] = get_projects
    template.globals["get_project_links"] = get_project_links

    dirname = os.path.join(docdir, "tasks")
    os.makedirs(dirname, exist_ok=True)
    for category, category_tasks in tasks.items():
        beamlines = CATEGORY_TO_BEAMLINES.get(category)

        with open(
            os.path.join(dirname, category_filename_stem(category) + ".md"),
            "w",
            encoding="utf-8",
        ) as f:

            f.write(
                template.render(
                    category=category,
                    tasks=category_tasks,
                    beamlines=beamlines,
                )
            )


def save_overview(docdir: str, tasks_per_category: Dict[str, int]) -> None:
    dirname = os.path.join(docdir, "tasks")
    os.makedirs(dirname, exist_ok=True)

    all_beamlines = set()
    for beamlines in CATEGORY_TO_BEAMLINES.values():
        all_beamlines |= set(beamlines)

    template = env.get_template("overview.md")
    template.globals["get_projects"] = get_projects
    template.globals["get_project_links"] = get_project_links
    template.globals["category_filename_stem"] = category_filename_stem

    with open(os.path.join(dirname, "overview.md"), "w", encoding="utf-8") as f:
        f.write(
            template.render(
                categories=list(CATEGORY_TO_BEAMLINES.keys()),
                tasks_per_category=tasks_per_category,
                all_beamlines=all_beamlines,
            )
        )


def main():
    tasks = create_task_list()
    docdir = os.path.join(os.path.dirname(__file__), "..", "doc")
    save_category_pages(tasks, docdir)
    tasks_per_category = {category: len(tasks) for category, tasks in tasks.items()}
    save_overview(docdir, tasks_per_category)


if __name__ == "__main__":
    main()
