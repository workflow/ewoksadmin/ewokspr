# {{category}} tasks

:::{admonition} How to use
:class: info

{% set projects = get_projects(category) %}
{% if projects %}
These tasks come from {{" ".join(get_project_links(projects))}}. It can be installed with

```python
pip install {{' '.join(projects)}}
```
{% else %}
_Under construction 🏗️_
{% endif %}

{% if beamlines %}
ℹ️ _These tasks are used at the following ESRF beamlines_: **{{', '.join(beamlines)}}**
{% endif %}

:::

{% for task in tasks %}

## {{ task['name'] }}
{% if task['description'] %}
{{ task['description'] }}
{% endif %}

:Identifier: `{{ task['task_identifier' ]}}`
:Task type: _{{ task['task_type'] }}_
:Required inputs:

    {{ task['required_input_names']|join(', ') }}
:Optional inputs:

    {{ task['optional_input_names']|join(', ') }}
:Outputs:

    {{ task['output_names']|join(', ') }}

{% endfor %}