:::{admonition} {{ all_beamlines|length }} ESRF beamlines use Ewoks to process their data!
:class: info

Discover {{ tasks_per_category.values()|sum }} workflow tasks below or use the search box
:::

::::{grid} 3
:gutter: 3

{% for category in categories %}
{% set projects = get_projects(category) %}

:::{grid-item-card}
:link: {{ category_filename_stem(category) }}.html

**{{ category }}**

^^^
{% if projects %}
⚙️ **{{ tasks_per_category.get(category, 0) }} tasks**
{% for project_link in get_project_links(projects) %}
  - {{ project_link }}
{% endfor %}
{% else %}
_Under construction 🏗️_
{% endif %}

:::

{% endfor %}
::::
