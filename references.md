# Literature on workflows

## Workflow management systems

 * [Database](https://s.apache.org/existing-workflow-systems): list of existing WMS's
 * [Blog post, July 29, 2020](https://kapernikov.com/a-comparison-of-data-processing-frameworks/): data vs. task drive WMS's
